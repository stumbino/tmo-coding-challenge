import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PriceQueryFacade } from '@coding-challenge/stocks/data-access-price-query';
import { Observable, BehaviorSubject } from 'rxjs';
import { MatMonthView, MatDatepicker} from '@angular/material/datepicker';
import { values } from 'lodash-es';

@Component({
  selector: 'coding-challenge-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnInit {
  stockPickerForm: FormGroup;
  symbol: string;
  period: string;
  startDate: string;
  endDate: Date;
  startDateBinding: string;
  endDateBinding: string;
  dateToPass: any;
  quotes$ = this.priceQuery.priceQueries$;

  timePeriods = [
    { viewValue: 'All available data', value: 'max' },
    { viewValue: 'Five years', value: '5y' },
    { viewValue: 'Two years', value: '2y' },
    { viewValue: 'One year', value: '1y' },
    { viewValue: 'Year-to-date', value: 'ytd' },
    { viewValue: 'Six months', value: '6m' },
    { viewValue: 'Three months', value: '3m' },
    { viewValue: 'One month', value: '1m' }
  ];
  onChangeDate($event){
    console.log($event);
  }
  constructor(private fb: FormBuilder, private priceQuery: PriceQueryFacade) {
    this.stockPickerForm = fb.group({
      symbol: [null, Validators.required],
      period: [null, Validators.required],
      startDate: [null, Validators.required],
      endDate: [null, Validators.required]
    });
    this.stockPickerForm.valueChanges.subscribe({
      next: value=>{
          if(value.startDate != null && value.endDate != null){
            if(new Date(value.startDate) > new Date(value.endDate)){
              this.stockPickerForm.get('endDate').setValue(value.startDate);
            }
          }
          this.fetchQuote();
    
        }
      
    })
    // this.stockPickerForm.subscribe(value => {
    //   System.out.println(symbol):
    // }) 
  }

 onDate(event) {
    console.log("event");
    console.log(event);

  }
  ngOnInit(){
    
  }


  fetchQuote() {
    if (this.stockPickerForm.valid) {
      const { symbol, period, startDate, endDate} = this.stockPickerForm.value;
      this.priceQuery.fetchQuote(symbol, period, startDate, endDate);
    }
  }

}
