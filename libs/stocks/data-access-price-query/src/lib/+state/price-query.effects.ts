import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import {
  StocksAppConfig,
  StocksAppConfigToken
} from '@coding-challenge/stocks/data-access-app-config';
import { Effect } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/nx';
import { map } from 'rxjs/operators';
import {
  FetchPriceQuery,
  PriceQueryActionTypes,
  PriceQueryFetched,
  PriceQueryFetchError
} from './price-query.actions';
import { PriceQueryPartialState } from './price-query.reducer';
import { PriceQueryResponse } from './price-query.type';

@Injectable()
export class PriceQueryEffects {
  @Effect() loadPriceQuery$ = this.dataPersistence.fetch(
    PriceQueryActionTypes.FetchPriceQuery,
    {
      run: (action: FetchPriceQuery, state: PriceQueryPartialState) => {
        return this.httpClient
          .post(
            '/api/getStocks',    {
             'symbol':`${action.symbol}`,
             'period':`${action.period}`
          }
          )
          .pipe(
            map(resp => {
               const startDate = new Date(`${action.startDate}`);
               const endDate = new Date(`${action.endDate}`);
               const stockMap = JSON.parse(JSON.stringify(resp));
               
              if(endDate != null){
                const dateRangeForStocks = stockMap.filter(res => {
                  const date = new Date(res.date);
                    if(date >= startDate && date <= endDate){
                      return res;
                    }
                });
                return new PriceQueryFetched(dateRangeForStocks as PriceQueryResponse[])
              }
              return new PriceQueryFetched(resp as PriceQueryResponse[])
            })
          );
      },

      onError: (action: FetchPriceQuery, error) => {
        return new PriceQueryFetchError(error);
      }
    }
  );

  constructor(
    @Inject(StocksAppConfigToken) private env: StocksAppConfig,
    private httpClient: HttpClient,
    private dataPersistence: DataPersistence<PriceQueryPartialState>
  ) {}
}
