/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 **/
import { Server } from 'hapi';
const req = require('request');
const init = async () => {
  const server = new Server({
    port: 3333,
    host: 'localhost'
  });
  server.route({
    method: 'GET',
    path: '/api/',
    handler: (request, h) => {
      return {
        hello: 'world'
      };
    }
  });

  //const symbol = `${encodeURIComponent(payload.symbol)}`;
  //const period = `${encodeURIComponent(payload.period)}`;
  //${this.env.apiURL}/beta/stock/${action.symbol}/chart/${action.period}?token=${this.env.apiKey},
  server.route({
    method: 'POST',
    path: '/api/getStocks',
    handler: function (request, h) {
        let payload = request.payload;
        const options = {
          url: `https://sandbox.iexapis.com/beta/stock/${encodeURIComponent(payload.symbol)}/chart/${encodeURIComponent(payload.period)}?token=Tpk_a64d06a237bd4ad7905af9fdf6cac539`,
          headers: {
              'User-Agent': 'request'
          }
      };
      return new Promise(function(resolve, reject) {
          req.get(options, function(err, resp, body) {
              if (err) {
                  reject(err);
              } else {
                  resolve(JSON.parse(body));
              }
          })
    })
  }
});

  await server.start();
  console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', err => {
  console.log(err);
  process.exit(1);
});

init();
